package com.example.formacion1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Formacion1Application {

	public static void main(String[] args) {
		SpringApplication.run(Formacion1Application.class, args);
	}

}
